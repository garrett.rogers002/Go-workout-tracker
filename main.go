package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"

	datepicker "github.com/sdassow/fyne-datepicker"
)

type workout struct {
	Date   time.Time
	Name   string
	Sets   int
	Reps   []int
	Weight float32
}

const workoutFile string = "workout_data.json"
const headerOffset = 1

var data []workout
var workoutURI fyne.URI

func makeCell(id widget.TableCellID, obj fyne.CanvasObject) {
	cell, ok := obj.(*widget.Label)
	if !ok {
		cell = widget.NewLabel("")
	}

	if id.Row > 0 && id.Row < len(data)+headerOffset {
		workout := data[id.Row-headerOffset]
		switch id.Col {
		case 0:
			cell.SetText(workout.Date.Format("2006-01-02"))
		case 1:
			cell.SetText(workout.Name)
		case 2:
			cell.SetText(strconv.Itoa(workout.Sets))
		case 3:
			cell.SetText(workout.repsAsString())
		case 4:
			cell.SetText(workout.weightAsString())
		}
	}

	header := []string{"Date", "Name", "Sets", "Reps", "Weight"}
	if id.Row == 0 {
		cell.SetText(header[id.Col])
	}
}

func main() {
	var a fyne.App
	var menuContent *fyne.Container

	if strings.TrimSpace(os.Getenv("WORKOUT_TEST")) == "1" {
		fmt.Println("Running in testing mode")
		a = app.NewWithID("com.garrett.workouttest")
	} else {
		a = app.New()
	}
	w := a.NewWindow("Workout Tracker")

	workoutURI = newURIFromRoot(a, workoutFile)
	data = jsonToWorkouts(loadBytes(workoutURI))

	menuContent = container.NewVBox(
		widget.NewButton("Goals", func() {
			fmt.Println("Clicked Goals")
		}),
		widget.NewButton("Notifications and Reminders", func() {
			fmt.Println("Clicked Notifications and Reminders")
		}),
	)
	menuContent.Hide()

	menuIcon := widget.NewButtonWithIcon("", theme.MenuIcon(), func() {
		if menuContent.Hidden {
			menuContent.Show()
		} else {
			menuContent.Hide()
		}
	})

	grid := widget.NewTable(
		func() (int, int) {
			return len(data) + headerOffset, 5
		},
		func() fyne.CanvasObject {
			return widget.NewLabel("")
		},
		makeCell,
	)
	grid.SetColumnWidth(0, 100)
	grid.SetColumnWidth(1, 100)
	grid.SetColumnWidth(2, 100)
	grid.SetColumnWidth(3, 100)

	addButton := widget.NewButtonWithIcon("", theme.ContentAddIcon(), func() {
		openAddWindow(a, grid)
	})

	// content := container.New(
	// 	layout.NewBorderLayout(nil, addButton, nil, nil),
	// 	container.NewScroll(grid),
	// 	addButton,
	// )

	// content := container.NewBorder(
	// 	nil,
	// 	container.NewHBox(
	// 		layout.NewSpacer(),
	// 		addButton,
	// 	),
	// 	container.NewVBox(menuIcon),
	// 	nil,
	// 	container.NewScroll(grid),
	// 	// addButton,
	// )
	content := container.NewBorder(
		nil,
		container.NewHBox(
			layout.NewSpacer(),
			addButton,
		),
		container.NewVBox(
			menuIcon,
			menuContent,
		),
		nil,
		container.NewScroll(grid),
	)

	// content := container.NewVBox( //why are layouts so hard
	// 	layout.NewStackLayout(), menuIcon,
	// 	)
	// container.New((container.NewScroll(grid))),

	// 		container.NewVBox(
	// 			// layout.NewSpacer(),
	// 			addButton,
	// 		),
	// 	),
	// )

	w.SetMaster()
	w.SetContent(content)

	w.Resize(fyne.NewSize(600, 400))
	w.ShowAndRun()
}

func openAddWindow(a fyne.App, grid *widget.Table) {
	newWindow := a.NewWindow("Add New Workout")

	dateEntry := widget.NewEntry()
	dateEntry.SetPlaceHolder(time.Now().Format("2006-01-02"))
	// dateEntry.SetText(time.Now().Format("2006-01-02"))
	// ----------------------------------------------------------------------------------------------------
	dateEntry.ActionItem = widget.NewButtonWithIcon("", theme.MoreHorizontalIcon(), func() {
		var d *dialog.CustomDialog

		when := time.Now()

		if dateEntry.Text == "" {
			when = time.Now()
		} else {
			t, err := time.Parse("2006/01/02", dateEntry.Text)
			if err == nil {
				when = t
			}
		}

		// dateEntry.SetText(time.Now().Format("2006-01-02"))

		datepicker := datepicker.NewDatePicker(when, time.Monday, func(when time.Time, ok bool) {
			if ok {
				dateEntry.SetText(when.Format("2006-01-02"))
			}
			d.Hide()
		})

		d = dialog.NewCustomWithoutButtons("Choose date and time", datepicker, newWindow)
		d.Show()
	})
	// if dateEntry.Text = {
	// 	dateEntry.SetText(time.Now().Format("2006-01-02"))
	// }

	// label := widget.NewLabelWithStyle("Demo", fyne.TextAlignCenter, fyne.TextStyle{Bold: true})
	// -----------------------------------------------------------------------------------------------------
	nameEntry := widget.NewEntry()
	nameEntry.SetPlaceHolder("Workout Name")
	setsEntry := widget.NewEntry()
	setsEntry.SetPlaceHolder("Number of Sets")
	repsEntry := widget.NewEntry()
	repsEntry.SetPlaceHolder("Reps (separated by commas)")
	weightEntry := widget.NewEntry()
	weightEntry.SetPlaceHolder("Amount of Weight")

	submitButton := widget.NewButton("Save Workout", func() {
		// repStrs := strings.Split(repsEntry.Text, ",")
		// reps := make([]int, len(repStrs))
		// for i, repStr := range repStrs {
		// 	rep, err := strconv.Atoi(strings.TrimSpace(repStr))
		// 	if err != nil {
		// 		fmt.Println("Error parsing rep:", err)
		// 		return
		// 	}
		// 	reps[i] = rep
		// }
		parsedSets, err := strconv.Atoi(setsEntry.Text)
		if err != nil {
			fmt.Println("Error with Atoi(): " + err.Error())
		}
		parsedDate, err := time.Parse("2006-01-02", dateEntry.Text)
		if err != nil {
			fmt.Println("Error with Parse(): " + err.Error())
		}
		parsedReps := make([]int, 0)
		repTokens := strings.Split(repsEntry.Text, ",")
		for _, token := range repTokens {
			rep, err := strconv.Atoi(token)
			if err != nil {
				fmt.Println("Error with Atoi(): " + err.Error())
			}
			parsedReps = append(parsedReps, rep)
		}
		parsedWeight, err := strconv.ParseFloat(weightEntry.Text, 32)
		if err != nil {
			fmt.Println("Error with ParseFloat(): " + err.Error())
		}

		workout := workout{
			Date:   parsedDate,
			Name:   nameEntry.Text,
			Sets:   parsedSets,
			Reps:   parsedReps,
			Weight: float32(parsedWeight),
		}
		data = append(data, workout)
		grid.Refresh()

		saveBytes(workoutURI, workoutsToJSON(data))
		newWindow.Close()
	})

	form := container.NewGridWithColumns(2,
		widget.NewLabel("Date:"), dateEntry,
		widget.NewLabel("Name:"), nameEntry,
		widget.NewLabel("Sets:"), setsEntry,
		widget.NewLabel("Reps:"), repsEntry,
		widget.NewLabel("Weight:"), weightEntry,
	)

	content := container.NewVBox(form, submitButton)
	newWindow.SetContent(content)
	newWindow.Resize(fyne.NewSize(640, 480))
	newWindow.Show()
}
