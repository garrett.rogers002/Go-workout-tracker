# Go Workout Tracker

Welcome to the Go Workout Tracker project (or "Wacker" for short)!

## Introduction

I'm Garrett, and this project is a part of my journey to learn more about Go programming. As a computer science student at Middle Georgia State University, I'm diving into the world of Go to understand how the language works, explore libraries, and get hands-on experience with UI development.

## Purpose

The primary goal of this project is self-education. I'm using this opportunity to:

- Learn the fundamentals of Go programming.
- Gain insights into how libraries, especially Fyne in this case, function.
- Develop skills in creating a user interface using Go.

## Project Structure

The project is in its early stages, and I'm experimenting with workout tracking and UI design. Expect changes and improvements as I learn more and expand the functionality.

## Contributions

While contributions are not the main focus right now, I welcome any feedback, suggestions, or insights from the Grindle. Feel free to open issues or reach out with your thoughts.

## Running the Project

Make sure you have Go installed on your machine. To run the project you have to do so in test mode, do the following:

```bash
cmd /C "set WORKOUT_TEST=1 && go run ."
